package com.agileai.crm.cxmodule;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface OrgInfoManage
        extends StandardService {
	void createVisitRecord(DataParam param);
	void createCustomerRecord(DataParam param,String custId);
	void changeStateRecord(DataParam param);
	List<DataRow> findCustomerRecords(DataParam param);
	List<DataRow> findOrgLabelsRecords(DataParam param);
	public void addSalesManRelation(DataParam param);
	public void assignedSaleRecord(DataParam param);
	public List<DataRow> findOrgSalesmanRecords(DataParam param);
	public void batchDeleteRecords(List<DataParam> paramList);
	public void batchCreateRecord(List<DataParam> paramList);
}
