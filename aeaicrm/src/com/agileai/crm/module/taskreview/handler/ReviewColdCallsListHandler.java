package com.agileai.crm.module.taskreview.handler;

import java.util.Date;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.ColdCallsManage;
import com.agileai.crm.cxmodule.TaskCycle8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.MasterSubListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class ReviewColdCallsListHandler
        extends MasterSubListHandler {
    public ReviewColdCallsListHandler() {
        super();
//        this.editHandlerClazz = ColdCallsManageEditHandler.class;
        this.serviceId = buildServiceId(ColdCallsManage.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		DataRow dataRow = getService().getTaskReviewRecord(param);
		String taskReviewId = "";
		if(dataRow != null && !dataRow.isEmpty()){
			String taskReviewState = dataRow.getString("TASK_REVIEW_STATE");
			this.setAttribute("TASK_REVIEW_STATE", taskReviewState);
			taskReviewId = dataRow.getString("TASK_REVIEW_ID");
			this.setAttribute("TASK_REVIEW_ID", taskReviewId);
		}
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findMasterRecords(new DataParam("TASK_REVIEW_ID",taskReviewId, "orgSalesman", param.get("SALE_ID")));
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
    protected void processPageAttributes(DataParam param) {
		initMappingItem("ORG_STATE", 
				FormSelectFactory.create("PER_STATE").getContent());
		initMappingItem("ORG_CLASSIFICATION", 
				FormSelectFactory.create("ORG_CLASSIFICATION").getContent());
		initMappingItem("TASK_FOLLOW_STATE", 
				FormSelectFactory.create("TASK_FOLLOW_STATUS").getContent());
    }

    protected void initParameters(DataParam param) {
    }
    
    public ViewRenderer doCreateTaskAction(DataParam param) {
		String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	String dateStr = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, new Date());
    	for(int i=0;i < idArray.length;i++){
    		String orgId = idArray[i];
    		DataRow dataRow = getService().getProCustRecord(new DataParam("ORG_ID",orgId));
    		DataParam myTasksParam = new DataParam();
    		myTasksParam.put("TASK_ID", KeyGenerator.instance().genKey());
    		myTasksParam.put("ORG_ID", orgId);
    		myTasksParam.put("CUST_ID", dataRow.get("CUST_ID"));
    		myTasksParam.put("TASK_REVIEW_ID", param.get("TASK_REVIEW_ID"));
    		myTasksParam.put("SALE_ID", dataRow.get("ORG_SALESMAN"));
    		myTasksParam.put("TASK_FOLLOW_STATE", "NoFollowUp");
    		myTasksParam.put("TASK_CLASS", "ColdCalls");
    		myTasksParam.put("TASK_CREATE_TIME", dateStr);
    		myTasksParam.put("TASK_FINISH_TIME", "");
    		myTasksParam.put("TASK_CUST_STATE", "");
    		getService().createMasterRecord(myTasksParam);
    	}
    	return prepareDisplay(param);
    }
    
	public ViewRenderer doSubmitPlanAction(DataParam param){
		param.put("TASK_REVIEW_STATE", "SubmitPlan");
		TaskCycle8ContentManage service = this.lookupService(TaskCycle8ContentManage.class);
		String taskReviewState = param.stringValue("TASK_REVIEW_STATE");
		String taskReviewId = param.stringValue("TASK_REVIEW_ID");
		service.updatePlanRecord(taskReviewId,taskReviewState);
		return prepareDisplay(param);
	}
	
	public ViewRenderer doConfirmPlanAction(DataParam param){
		param.put("TASK_REVIEW_STATE", "ConfirmPlan");
		TaskCycle8ContentManage service = this.lookupService(TaskCycle8ContentManage.class);
		service.updateSummaryRecord(param);
		return prepareDisplay(param);
	}
	
	public ViewRenderer doRepulsePlanAction(DataParam param){
		param.put("TASK_REVIEW_STATE", "Init");
		TaskCycle8ContentManage service = this.lookupService(TaskCycle8ContentManage.class);
		service.updateSummaryRecord(param);
		return prepareDisplay(param);
	}
	
	public ViewRenderer doDeleteAction(DataParam param){
		storeParam(param);
		getService().deleteClusterRecords(param);	
		return prepareDisplay(param);
	}

    protected ColdCallsManage getService() {
        return (ColdCallsManage) this.lookupService(this.getServiceId());
    }
}
