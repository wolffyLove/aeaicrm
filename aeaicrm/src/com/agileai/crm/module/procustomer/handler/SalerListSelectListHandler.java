package com.agileai.crm.module.procustomer.handler;

import com.agileai.crm.cxmodule.SalerListSelect;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;

public class SalerListSelectListHandler
        extends PickFillModelHandler {
    public SalerListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(SalerListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }
    
    protected SalerListSelect getService() {
        return (SalerListSelect) this.lookupService(this.getServiceId());
    }
}
