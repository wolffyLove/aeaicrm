<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>我的客户</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="create"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input id="&nbsp;" value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td></aeai:previlege>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td></aeai:previlege>
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input id="detailImgBtn" value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege>
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input id="delete" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;名称<input id="custName" label="名称" name="custName" type="text" value="<%=pageBean.inputValue("custName")%>" size="24" class="text" />

&nbsp;级别<select id="custLevel" label="行业" name="custLevel" class="select" onchange="doQuery()"><%=pageBean.selectValue("custLevel")%></select>

&nbsp;状态<select id="custState" label="规模" name="custState" class="select" onchange="doQuery()"><%=pageBean.selectValue("custState")%></select>

&nbsp;沟通效果<select id="visitEffect" label="沟通效果" name="visitEffect" class="select" onchange="doQuery()"><%=pageBean.selectValue("visitEffect")%></select>

&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList"
retrieveRowsCallback="process"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{CUST_ID:'${row.CUST_ID}'});controlUpdateBtn('${row.CUST_STATE}');refreshConextmenu()" onclick="selectRow(this,{CUST_ID:'${row.CUST_ID}'});controlUpdateBtn('${row.CUST_STATE}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CUST_NAME" title="客户名称"   />
	<ec:column width="100" property="CUST_LEVEL" title="级别"   mappingItem="CUST_LEVEL"/>
	<ec:column width="100" property="VISIT_DATE" title="拜访时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="VISIT_USER_NAME" title="拜访人员"   />
	<ec:column width="100" property="VISIT_EFFECT" title="沟通效果"  mappingItem="VISIT_EFFECT"/>
	<ec:column width="100" property="CUST_STATE" title="状态"   mappingItem="CUST_STATE"/>
</ec:row>
</ec:table>
<input type="hidden" name="CUST_ID" id="CUST_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('CUST_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
function controlUpdateBtn(stateResult){
	if(stateResult =='init'){
		enableButton("editImgBtn");
		enableButton("detailImgBtn");
		enableButton("delete");
	}else if(stateResult =='Submit'){
		disableButton("editImgBtn");
		enableButton("detailImgBtn");
		disableButton("delete");
	}else{
		disableButton("editImgBtn");
		enableButton("detailImgBtn");
		disableButton("delete");
	}	
}
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
