<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.agileai.hotweb.domain.PageBean"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>任务审查</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function changeTab(tabId,userId){
    $('#currentTabId').val(tabId);
    $('#currentUserId').val(userId);
    doSubmit({actionType:'prepareDisplay'});
}
function changeTabId(tabId){
	$('#tableId').val(tabId);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
</div>
<div id="__ParamBar__">
<table class="queryTable">
<tr>
  <td>
    &nbsp;起止日期&nbsp;
<input name="TC_BEGIN" type="text" class="text" id="TC_BEGIN" value="<%=pageBean.inputValue("TC_BEGIN")%>" size="10" readonly="readonly"/>
-
<input name="TC_END" type="text" class="text" id="TC_END" value="<%=pageBean.inputValue("TC_END")%>" size="10" readonly="readonly" />
    &nbsp;<input type="button" name="button" id="button" value="上一周" class="formbutton" onclick="doSubmit({actionType:'beforeWeek'})" /> 
    &nbsp;<input type="button" name="button" id="button" value="本周" class="formbutton" onclick="doSubmit({actionType:'currentWeek'})" />
    &nbsp;<input type="button" name="button" id="button" value="下一周" class="formbutton" onclick="doSubmit({actionType:'nextWeek'})" />
  </td>
</tr>
</table>
</div>
<br/>
<div class="photobg1" id="tabHeader">
<%  List saleRecords = (List)pageBean.getAttribute("saleRecords");
	pageBean.setRsList(saleRecords);
	int paramSize = pageBean.listSize();
	for(int i=0;i<paramSize;i++){%>
    <div class="newarticle1" onclick="changeTab('<%=i%>','<%=pageBean.inputValue(i,"USER_ID")%>')" ><%=pageBean.inputValue(i,"USER_NAME")%>
    </div>
<%}%>
</div>
<%  
for(int i=0;i<paramSize;i++){
%>
<div class="photobox newarticlebox" id="Layer<%=i%>" style="height:427px;;display:none;overflow:hidden;">
<% 
if (pageBean.inputValue("currentUserId").equals(pageBean.inputValue(i,"USER_ID"))){
%>
<iframe id="UserFrame" src="index?ReviewSummary8ContentList&TC_ID=<%=pageBean.inputValue("currentWeekId")%>&SALE_ID=<%=pageBean.inputValue("currentUserId")%>&TC_BEGIN=<%=pageBean.inputValue("TC_BEGIN")%>&TC_END=<%=pageBean.inputValue("TC_END")%>&currentSubTableId=<%=pageBean.inputValue("tableId")== ""?"ColdCalls":pageBean.inputValue("tableId")%>" width="100%" height="450" frameborder="0" scrolling="auto"></iframe>
<%}%>
</div>
<% } %>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="currentUserId" name="currentUserId" value="<%=pageBean.inputValue("currentUserId")%>"/>
<input type="hidden" id="currentTabId" name="currentTabId" value="<%=pageBean.inputValue("currentTabId")%>" />
<input type="hidden" id="currentWeekId" name="currentWeekId" value="<%=pageBean.inputValue("currentWeekId")%>" />
<input type="hidden" id="tableId" name="tableId" value="<%=pageBean.inputValue("tableId")%>" />
</form>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(<%=pageBean.inputValue("currentTabId")%>);
$(function(){
	resetTabHeight(100);
	$("#UserFrame").height($("#form1").height()-100);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>